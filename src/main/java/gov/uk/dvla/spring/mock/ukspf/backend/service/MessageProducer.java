package gov.uk.dvla.spring.mock.ukspf.backend.service;

import gov.uk.dvla.spring.mock.ukspf.backend.model.req.DataBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Component
@Slf4j
public class MessageProducer {

  private final KafkaTemplate<String, DataBody> kafkaTemplate;

  private final String topicName;

  public MessageProducer(KafkaTemplate<String, DataBody> kafkaTemplate,
                         @Value(value = "${kafka.message.topic.name}") String topicName ){
    this.kafkaTemplate = kafkaTemplate;
    this.topicName = topicName;
  }

  public void sendMessage(DataBody message) {

    ListenableFuture<SendResult<String, DataBody>> future = kafkaTemplate.send(topicName, message);

    future.addCallback(new ListenableFutureCallback<>() {
      @Override
      public void onFailure(Throwable throwable) {
        log.info("Failure");
      }
      @Override
      public void onSuccess(SendResult<String, DataBody> objectDataBodySendResult) {
        log.info("Success");
      }
    });
  }
}
