package gov.uk.dvla.spring.mock.ukspf.backend;

import gov.uk.dvla.spring.mock.ukspf.backend.service.ViewRepository;
import gov.uk.dvla.spring.mock.ukspf.backend.model.req.DataBody;
import gov.uk.dvla.spring.mock.ukspf.backend.service.DbData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MessageConsumer {

  private final ViewRepository viewRepository;

  public MessageConsumer(ViewRepository viewRepository){
    this.viewRepository = viewRepository;
  }

  @KafkaListener(topics = "creds",
      containerFactory = "kafkaListenerContainerFactory",
      groupId = "data")
  public void listenGroupData(DataBody message) {
    log.info("Received Message");
    DbData dbData = new DbData(message.getFirstName(), message.getSurname());
    viewRepository.insert(dbData);
    log.info("Successfully inserted");
  }
}
