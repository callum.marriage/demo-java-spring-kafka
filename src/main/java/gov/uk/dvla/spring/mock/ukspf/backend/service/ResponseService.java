package gov.uk.dvla.spring.mock.ukspf.backend.service;

import gov.uk.dvla.spring.mock.ukspf.backend.model.res.ResData;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ResponseService {

  private final ViewRepository repository;

  public ResponseService(ViewRepository repository){
    this.repository = repository;
  }

  public List<ResData> retrieveResData(){
    List<DbData> dbData = repository.findAll();
    return dbData.stream()
        .map(ResDataMapper::mapDbToRes)
        .collect(Collectors.toList());
  }
}
