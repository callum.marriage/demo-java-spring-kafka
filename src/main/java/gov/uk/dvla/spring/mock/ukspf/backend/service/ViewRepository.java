package gov.uk.dvla.spring.mock.ukspf.backend.service;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ViewRepository extends MongoRepository<DbData, String> {


  Optional<DbData> findByFirstNameAndSurname(String firstName, String surname);

}
