package gov.uk.dvla.spring.mock.ukspf.backend;

import gov.uk.dvla.spring.mock.ukspf.backend.model.res.ResData;
import gov.uk.dvla.spring.mock.ukspf.backend.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ResponseController {

  private final ResponseService responseService;

  public ResponseController(ResponseService responseService) {
    this.responseService = responseService;
  }

  @GetMapping(value = {"/data"})
  @ResponseStatus(HttpStatus.OK)
  public List<ResData> retrieveData() {
    return responseService.retrieveResData();
  }
}
