package gov.uk.dvla.spring.mock.ukspf.backend.service;

import gov.uk.dvla.spring.mock.ukspf.backend.model.res.ResData;

public class ResDataMapper {

  private ResDataMapper(){

  }

  public static ResData mapDbToRes(DbData dbData){
    return new ResData(dbData.getFirstName(), dbData.getSurname());
  }
}
