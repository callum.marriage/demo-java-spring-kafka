package gov.uk.dvla.spring.mock.ukspf.backend.service;


import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "test")
public class DbData {

  @Indexed(unique = true)
  private String id;

  private String firstName;

  private String surname;

  public DbData(String firstName, String surname){
    this.firstName = firstName;
    this.surname = surname;
  }

  public DbData() {
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }
}
