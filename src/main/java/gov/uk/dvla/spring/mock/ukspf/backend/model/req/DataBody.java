package gov.uk.dvla.spring.mock.ukspf.backend.model.req;


import com.fasterxml.jackson.annotation.JsonProperty;

public class DataBody {

  @JsonProperty("firstName")
  private String firstName;

  @JsonProperty("surname")
  private String surname;

  public DataBody(String firstName, String surname) {
    this.firstName = firstName;
    this.surname = surname;
  }

  public DataBody() {
  }

  public String getFirstName() {
    return firstName;
  }

  public String getSurname() {
    return surname;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }
}
