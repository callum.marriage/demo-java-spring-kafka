package gov.uk.dvla.spring.mock.ukspf.backend;

import gov.uk.dvla.spring.mock.ukspf.backend.model.req.DataBody;
import gov.uk.dvla.spring.mock.ukspf.backend.model.res.ResData;
import gov.uk.dvla.spring.mock.ukspf.backend.service.MessageProducer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DataController {

  private final MessageProducer messageProducer;

  public DataController(MessageProducer messageProducer){
    this.messageProducer = messageProducer;
  }

  @PostMapping("/data")
  public ResponseEntity<ResData> postData(@RequestBody DataBody requestData){
    messageProducer.sendMessage(requestData);
    return ResponseEntity
        .status(HttpStatus.CREATED).build();
  }
}
