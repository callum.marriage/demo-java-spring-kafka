FROM gcr.io/distroless/java:11
EXPOSE 5001

COPY target/spring-mock-ukspf-backend-*.jar /spring-mock-ukspf-backend.jar
ENTRYPOINT ["java", "-jar",  "/spring-mock-ukspf-backend.jar"]
